namespace :util do
  desc 'List roles'
  task :list_roles do
    roles(:all).map{ |server| server.roles_array }.flatten.uniq.sort.each do |role|
      puts role
    end
  end

  desc 'List servers'
  task :list_servers do
    puts "web"
    roles(:web).map(&:hostname).sort.each { |hostname| puts hostname }
    puts "app"
    roles(:app).map(&:hostname).sort.each { |hostname| puts hostname }
    puts "app_static"
    roles(:app_static).map(&:hostname).sort.each { |hostname| puts hostname }
    puts "db"
    roles(:db).map(&:hostname).sort.each { |hostname| puts hostname }
    puts "all"
    roles(:all).map(&:hostname).sort.each { |hostname| puts hostname }
  end

end
